"use strict";
console.log("IndexControllerSpec.js");
describe("IndexController", function () {
    var log, location, controller, httpBackend, vm;

    beforeEach(module("app.index"));
    // beforeEach(module("app.todosService"));

    beforeEach(inject(function ($controller, $httpBackend, $log, $location) {
        location = $location;
        log = $log;
        // todosService = _todosService_;
        httpBackend = $httpBackend;
        controller = $controller("IndexController", {});
        vm = controller;
    }));

    it("should be defined", function () {
        expect(controller).toBeDefined();
    });
});
