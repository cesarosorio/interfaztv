(function () {
  console.log("appRoute.js");
    'use strict';

    angular.module('app')

            .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function config($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.when('', '/dashboard');
      $urlRouterProvider.when('/', '/dashboard');
      $urlRouterProvider.otherwise('/');
      $stateProvider
      .state('root', {
          abstract: true,
          url: '/',
          data: {
              title: 'Home',
              breadcrumb: 'Home'
          },
          views: {
              'header': {
                  templateUrl: 'core/navigation/headerView.html',
                  controller: 'HeaderController',
                  controllerAs: 'HC'
              },
              'mjsusuarios': {
                  templateUrl: 'core/mjsusuarios/mjsusuariosView.html',
                  controller: 'MjsusuariosController',
                  controllerAs: 'MU'
              },
              'slidertv': {
                  templateUrl: 'core/slidertv/slidertvView.html',
                  controller: 'SlidertvController',
                  controllerAs: 'ST'
              },
              'cajitas': {
                  templateUrl: 'core/cajitas/cajitasView.html',
                  controller: 'CajitasController',
                  controllerAs: 'CA'
              },
              'footer': {
                  templateUrl: 'core/navigation/footerView.html',
                  controller: 'FooterController',
                  controllerAs: 'FC'
              }
          }
      })
      .state('root.dashboard', {
          url: 'dashboard',
          data: {
              title: 'Dashboard',
              breadcrumb: 'dashboard'
          },
          views: {
              'dashboard@': {
                  templateUrl: 'core/dashboard/dashboardView.html',
                  controller: 'DashboardController',
                  controllerAs: 'DA'
              }
          }
      });
    }
})();
