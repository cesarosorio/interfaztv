(function () {
  console.log("app.js");
    'use strict';

    angular.module('app', [
        'ui.router',
        'app.index',
        // 'app.json',
        // 'app.mountains.details',
        // 'app.mountains.list',
        // 'app.nav.breadcrumbs',
        'app.nav.footer',
        'app.nav.header',
        'app.mjsusuarios',
        'app.slidertv',
        'app.dashboard',
        'app.cajitas',
        // 'app.nav.menu',
        // 'app.todos.new',
        // 'app.todos.list',
        'app.directives.datepicker',
        'app.directives.about',
        'app.filters'
    ]);
})();
